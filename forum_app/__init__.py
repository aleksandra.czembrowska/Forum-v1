from flask import Flask, render_template, session, request, redirect, url_for, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_assets import Environment
from flask_migrate import Migrate
from flask_restful import Api
from flask_jwt_extended import JWTManager
from flask_mail import Mail
from sassutils.wsgi import SassMiddleware

app = Flask(__name__)

app.debug=True
app.config.from_object('forum_app.config.DevelopmentConfig')

db = SQLAlchemy(app)
assets = Environment(app)
migrate = Migrate(app, db)
api = Api(app)
jwt = JWTManager(app)

# scss setup
app.wsgi_app = SassMiddleware(app.wsgi_app, {
    'forum_app': ('static/sass', 'static/css/compiled', '/static/css/compiled', False)
})

# endpoints that don't require logging in
allowedEndpoints = (
    'login',
    'logout', 
    None, 
    'register',
    'user_login_api'
    )

#register blueprints
from forum_app.modules.auth.controllers import auth_module

app.register_blueprint(auth_module)

# API section
from forum_app.modules.api import resources
api.add_resource(resources.UserRegistration, '/api/auth/register')
api.add_resource(resources.UserLogin, '/api/auth/login')
api.add_resource(resources.UserLogoutAccess, '/api/auth/logout/access')
api.add_resource(resources.UserLogoutRefresh, '/api/auth/logout/refresh')
api.add_resource(resources.TokenRefresh, '/api/token/refresh')
api.add_resource(resources.AllUsers, '/api/users')
api.add_resource(resources.SecretResource, '/api/secret')
api.add_resource(resources.Translations, '/api/translations')


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return models.RevokedTokenModel.is_jti_blacklisted(jti)


@app.before_request
def before_request():
    if (
            not session.get('user_id') and # not logged in
            (request.endpoint not in allowedEndpoints) and # not in allowed endpoits
            (request.endpoint.split('.')[-1] != 'static')  # not static file
    ):
        return redirect(url_for('login'))



#import assets
import forum_app.assets