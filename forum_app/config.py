import os
import configparser


#config
AVATAR_VALID_EXTENTIONS = ['jpg', 'png', 'jpeg']
ATTACHMENT_VALID_EXTENTIONS = AVATAR_VALID_EXTENTIONS + ['pdf', 'doc', 'docx']

#DATABASE CONFIGURATION SCOPE
config =configparser.ConfigParser()
with open('forum_app/app_config.ini', 'r', encoding='utf-8') as f:
    config.read_file(f)
    db_config = {
        'login':config['DATABASE']['login'],
        'password':config['DATABASE']['password'],
        'url':config['DATABASE']['url'],
        'database':config['DATABASE']['db']
    }

    secrets = {
        'secret_key':config['SECRETS']['secret_key'],
        'jwt_secret_key':config['SECRETS']['jwt_secret_key']
    }



DB_URL = 'postgresql://{user}:{password}@{url}/{db}'.format(
    user=db_config['login'], password=db_config['password'], url=db_config['url'], db=db_config['database'])


class Config:
    SQLALCHEMY_DATABASE_URI = DB_URL
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = secrets['secret_key'] 
    MAKO_TRANSLATE_EXCEPTIONS = False
    ASSETS_AUTO_BUILD = True
    JSON_SORT_KEYS = False
    ASSETS_DEBUG = False
    CORS_HEADERS = 'Content-Type'
    JWT_SECRET_KEY = secrets['jwt_secret_key']
    JWT_BLACKLIST_ENABLED = True
    JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']
    

class ProductionConfig(Config):
    ENV = 'production'
    DEBUG = False
    TESTING = False
    # USE_X_SENDFILE = True # ?


class DevelopmentConfig(Config):
    ENV = 'development'
    DEBUG = True
    TESTING = True
    SEND_FILE_MAX_AGE_DEFAULT = 0  # forbid caching
    TEMPLATES_AUTO_RELOAD = True


class TestConfig(Config):
    DEBUG = True
    TESTING = True
    WTF_CSRF_ENABLED = False